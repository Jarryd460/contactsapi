﻿using System.Web.Http;

namespace ContactsAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services


            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );            

            config.InitializeCustomWebHooks();
            config.InitializeCustomWebHooksApis();
            config.InitializeReceiveCustomWebHooks();

            config.Filters.Add(new Authorisation.Authorisation());
            config.Filters.Add(new Authorisation.Authentication());
        }
    }
}
