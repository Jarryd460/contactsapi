﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace ContactsAPI.Authorisation
{
    public class Authorisation : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity("test"), null);
        }
    }
}