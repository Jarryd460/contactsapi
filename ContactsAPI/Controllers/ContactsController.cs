﻿using BudgetAPI.Repositories;
using ContactsAPI.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;

namespace BudgetAPI.Controllers
{
    public class ContactsController : ApiController
    {
        private readonly IContactsRepository _contactsRepository;

        public ContactsController()
        {
            _contactsRepository = new ContactsRepository();
        }

        // GET api/contacts
        [HttpGet]
        public List<ContactModel> Get()
        {
            return _contactsRepository.Contacts();
        }

        // GET api/contacts/5
        [HttpGet]
        public IHttpActionResult Get(Guid id)
        {
            ContactModel contact = _contactsRepository.ContactByID(id);
            if (contact == null)
            {
                return NotFound();
            }

            return Ok(contact);
        }

        // POST api/contacts
        [HttpPost]
        public async Task<IHttpActionResult> Post([FromBody]ContactModel contactToAdd)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _contactsRepository.AddContact(contactToAdd);

            await this.NotifyAllAsync("contact.created", contactToAdd);

            return CreatedAtRoute("DefaultApi", new { id = contactToAdd.ID }, contactToAdd);
        }

        // PUT api/contacts/5
        [HttpPut]
        public async Task<IHttpActionResult> Put(Guid id, [FromBody]ContactModel updateContact)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != updateContact.ID)
            {
                return BadRequest();
            }

            ContactModel category = _contactsRepository.ContactByID(id);

            if (category == null)
            {
                return NotFound();
            }

            _contactsRepository.UpdateContact(updateContact);

            await this.NotifyAllAsync("contact.updated", updateContact);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE api/contacts/5
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(Guid id)
        {
            ContactModel category = _contactsRepository.ContactByID(id);

            if (category == null)
            {
                return NotFound();
            }

            _contactsRepository.DeleteContact(id);

            await this.NotifyAllAsync("contact.deleted", category);

            return Ok();
        }
    }
}