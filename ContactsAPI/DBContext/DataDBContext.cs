﻿using ContactsAPI.Models;
using System.Data.Entity;

namespace BudgetAPI.DBContext
{

    public class WebHooksDbContext : DbContext
    {
        public WebHooksDbContext() : base("ConnectionString") { }

        public DbSet<ContactModel> Contacts { get; set; }
    }

}
