﻿using Microsoft.AspNet.WebHooks;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace ContactsAPI.Providers
{
    public class WebHookProvider : IWebHookFilterProvider
    {
        private readonly Collection<WebHookFilter> filters = new Collection<WebHookFilter>()
        {
            new WebHookFilter() { Name = "contact.created", Description = "A contact was created" },
            new WebHookFilter() { Name = "contact.updated", Description = "A contact was updated" },
            new WebHookFilter() { Name = "contact.deleted", Description = "A contact was deleted" }
        };

        public Task<Collection<WebHookFilter>> GetFiltersAsync()
        {
            return Task.FromResult(this.filters);
        }
    }
}