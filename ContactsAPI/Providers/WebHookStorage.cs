﻿using ContactsAPI.Repositories.WebHooks;
using Microsoft.AspNet.WebHooks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ContactsAPI.Providers
{
    public class WebHookStorage : IWebHookStore
    {
        private readonly WebHooksRepository _webHookRepository = null;

        public WebHookStorage()
        {
            _webHookRepository = new WebHooksRepository();
        }

        public Task DeleteAllWebHooksAsync(string user)
        {
            if (!string.IsNullOrWhiteSpace(user))
            {
                // Do the insert logic as you wish, here i am using a WebHook Repository 
                // to insert the details to SQL Server.The web hook repository uses  
                // EntityFrameWork to insert the web hook details to DB.
                bool isDeleted = _webHookRepository.DeleteAllUserWebHooks(user);
                StoreResult result = isDeleted ? StoreResult.Success : StoreResult.Conflict;
                return Task.FromResult(result);
            }
            else
            {
                throw new Exception("Values expected for either user or webHook");
            }
        }

        public Task<StoreResult> DeleteWebHookAsync(string user, string id)
        {
            if (!string.IsNullOrWhiteSpace(user) && !string.IsNullOrEmpty(id))
            {
                // Do the insert logic as you wish, here i am using a WebHook Repository 
                // to insert the details to SQL Server.The web hook repository uses  
                // EntityFrameWork to insert the web hook details to DB.
                bool isDeleted = _webHookRepository.DeleteWebHook(user, id);
                StoreResult result = isDeleted ? StoreResult.Success : StoreResult.Conflict;
                return Task.FromResult(result);
            }
            else
            {
                throw new Exception("Values expected for either user or webHook");
            }
        }

        public Task<ICollection<WebHook>> GetAllWebHooksAsync(string user)
        {
            if (!string.IsNullOrWhiteSpace(user))
            {
                // Do the insert logic as you wish, here i am using a WebHook Repository 
                // to insert the details to SQL Server.The web hook repository uses  
                // EntityFrameWork to insert the web hook details to DB.
                ICollection<WebHook> webHooks = _webHookRepository.GetAllUserWebHooks(user);
                return Task.FromResult(webHooks);
            }
            else
            {
                throw new Exception("Values expected for either user or webHook");
            }
        }

        public Task<StoreResult> InsertWebHookAsync(string user, WebHook webHook)
        {
            if (!string.IsNullOrWhiteSpace(user) && webHook != null)
            {
                // Do the insert logic as you wish, here i am using a WebHook Repository 
                // to insert the details to SQL Server.The web hook repository uses  
                // EntityFrameWork to insert the web hook details to DB.
                bool isInserted = _webHookRepository.AddWebHook(user, webHook);
                StoreResult result = isInserted ? StoreResult.Success : StoreResult.Conflict;
                return Task.FromResult(result);
            }
            else
            {
                throw new Exception("Values expected for either user or webHook");
            }
        }

        public Task<WebHook> LookupWebHookAsync(string user, string id)
        {
            throw new NotImplementedException();
        }

        public Task<ICollection<WebHook>> QueryWebHooksAcrossAllUsersAsync(IEnumerable<string> actions, Func<WebHook, string, bool> predicate)
        {
            throw new NotImplementedException();
        }

        public Task<ICollection<WebHook>> QueryWebHooksAsync(string user, IEnumerable<string> actions, Func<WebHook, string, bool> predicate)
        {
            throw new NotImplementedException();
        }

        public Task<StoreResult> UpdateWebHookAsync(string user, WebHook webHook)
        {
            if (!string.IsNullOrWhiteSpace(user) && webHook != null)
            {
                // Do the insert logic as you wish, here i am using a WebHook Repository 
                // to insert the details to SQL Server.The web hook repository uses  
                // EntityFrameWork to insert the web hook details to DB.
                bool isUpdated = _webHookRepository.UpdateWebHook(user, webHook);
                StoreResult result = isUpdated ? StoreResult.Success : StoreResult.Conflict;
                return Task.FromResult(result);
            }
            else
            {
                throw new Exception("Values expected for either user or webHook");
            }
        }
    }
}