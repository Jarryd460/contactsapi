﻿using BudgetAPI.DBContext;
using ContactsAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BudgetAPI.Repositories
{
    public class ContactsRepository: IContactsRepository
    {
   
        private readonly WebHooksDbContext _dbContext;

        public ContactsRepository()
        {
            _dbContext = new WebHooksDbContext();
        }

        public List<ContactModel> Contacts()
        {
            return _dbContext.Contacts.ToList();
        }

        public ContactModel ContactByID(Guid id)
        {
            return _dbContext.Contacts.Find(id);
        }

        public void AddContact(ContactModel contact)
        {
            _dbContext.Contacts.Add(contact);
            _dbContext.SaveChanges();
        }

        public void UpdateContact(ContactModel contacts)
        {
            _dbContext.Entry<ContactModel>(contacts).State = EntityState.Modified;
            _dbContext.SaveChanges();
        }

        public void DeleteContact(Guid id)
        {
            _dbContext.Contacts.Remove(_dbContext.Contacts.Find(id));
            _dbContext.SaveChanges();
        }
    }
}
