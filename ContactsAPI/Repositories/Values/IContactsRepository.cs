﻿using ContactsAPI.Models;
using System;
using System.Collections.Generic;

namespace BudgetAPI.Repositories
{
    public interface IContactsRepository
    {
        List<ContactModel> Contacts();
        ContactModel ContactByID(Guid id);
        void AddContact(ContactModel category);
        void UpdateContact(ContactModel category);
        void DeleteContact(Guid id);

    }
}
