﻿using Microsoft.AspNet.WebHooks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContactsAPI.Repositories.WebHooks
{
    public class WebHooksRepository
    {
        public ICollection<WebHook> GetAllUserWebHooks(string user)
        {
            throw new NotImplementedException();
        }

        public WebHook GetWebHook(string user, string id)
        {
            throw new NotImplementedException();
        }

        public ICollection<WebHook> GetWebHooksAcrossAllUsers(IEnumerable<string> actions, Func<WebHook, string, bool> predicate)
        {
            throw new NotImplementedException();
        }

        public ICollection<WebHook> GetWebHooks(string user, IEnumerable<string> actions, Func<WebHook, string, bool> predicate)
        {
            throw new NotImplementedException();
        }

        public bool AddWebHook(string user, WebHook webHook)
        {
            throw new NotImplementedException();
        }

        public bool UpdateWebHook(string user, WebHook webHook)
        {
            throw new NotImplementedException();
        }

        public bool DeleteWebHook(string user, string id)
        {
            throw new NotImplementedException();
        }

        public bool DeleteAllUserWebHooks(string user)  
        {
            throw new NotImplementedException();
        }
    }
}